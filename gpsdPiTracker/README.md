Welcome to pygpspi.

This is a project using the Raspberry PI along with Adafruit's
Ultimate GPS Breakout Board.

This utilizes UART connection and GPSD for Processing

http://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi

This is a great tutorial that came out after my first version of reading directly from serial
which I reversed engineered the Arduino Library for, it has the commands specific to
the Ultimate GPS for type of sentence and update frequency.

This is designed to post to a webservice, but the serial code in my other directory has
a UDP sender as well, but the server side dies the parsing of the full sentence right now,
so a new listener would need to be written to handle the already parsed data from GPSD

You will also see a need for Configuration files and log files

Feel free to ask any questions
dnetman99@gmail.com

