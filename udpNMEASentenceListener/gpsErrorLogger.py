#!/usr/bin/python -tt
#gpslogger.py
import logging
import logging.handlers

errorlogger = logging.getLogger('gpserrorlogger')
#errorhdlr = logging.FileHandler('/var/log/gpserror.log')
LOG_FILE = '/var/log/gpserror.log'
errorlogger.setLevel(logging.INFO)
errorhdlr = logging.handlers.RotatingFileHandler(LOG_FILE, maxBytes=1024, backupCount=50)
errorformatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
errorhdlr.setFormatter(errorformatter)
errorlogger.addHandler(errorhdlr)


class gpserrorlog:


	def __init__(self, logtype, message):
		self.logtype = logtype
		self.logmessage = message
		if self.logtype == 'ERROR':
			logger.error(self.logmessage)
		else:
			logger.info(self.logmessage)
