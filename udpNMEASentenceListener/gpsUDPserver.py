#!/usr/bin/python -tt

import ConfigParser
import urllib2, os
#from serialGetGPS import startSerialCapture
from michelle import myUDPServer
import sys, time
from daemon import Daemon
from gpsLogger import gpsservicelog
from gpsErrorLogger import gpserrorlog

class MyDaemon(Daemon):

    def run(self):
        istatus = False
        config = ConfigParser.RawConfigParser()
        config.read('/etc/gpstrack.conf')
        gpsunit = config.get("parms", "unitnumber")
        updateint = config.get("parms", "update")
        while istatus == False:
            try:
                response=urllib2.urlopen('http://www.google.com',timeout=2)
                istatus = True
				##print 'Internet up starting serial Capture'
                gpsservicelog('INFO', 'Internet up starting serial Capture')
                try:
                    myUDPServer('108.166.178.166',5000)
                except Exception as e:
                    gpserrorlog('ERROR', 'Listener Start Error:\n'+ str(e))

            except urllib2.URLError as err:
                pass
                istatus = False
                print 'No internet still testing before serial capture starts'
                gpsservicelog('ERROR', 'No internet still testing before serial capture starts')
                time.sleep(1)


if __name__ == "__main__":
	daemon = MyDaemon('/var/run/udpsocketserver.pid')
	if len(sys.argv) == 2:
		if 'start' == sys.argv[1]:
			gpsservicelog('INFO', 'GPS Server Started')
			daemon.start()
		elif 'stop' == sys.argv[1]:
			gpsservicelog('INFO', 'GPS Server Stopped')
			daemon.stop()
		elif 'restart' == sys.argv[1]:
			gpsservicelog('INFO', 'GPS Server Restarting')
			daemon.restart()
		else:
			print "Unknown command"
			sys.exit(2)
		sys.exit(0)
	else:
		print "usage: %s start|stop|restart" % sys.argv[0]
		sys.exit(2)